/* png_test.c - program to test writing of png files
   jdietrch
*/

#include <stdio.h>
#include <stdlib.h>
#include <iostream.h>
#include <png.h>

//pngwrite(int**windowbuffer, int*SETTINGS){
// Must pass w and h...
// Must pass filename
// got it? pngwrite(yourstructure, w, h, "my.png");
/// show me where the file is.

int pngwrite(pixel *windowbuffer, int newwidth, int newheight, char * newfilename){
    FILE *pngout;			//file pointer.
    png_structp write_ptr;		//write pointer?
    png_infop info_ptr;			//info pointer?
    png_uint_32 width = newwidth;	//width
    png_uint_32 height = newheight;	//height

    /* calculate number of bytes needed for each row assuming 8-bit RGB */
    png_uint_32 rowbytes = ((width * 8 * 3 + 7) >> 3);
//    cout << rowbytes << endl;
    png_uint_32 k;
    png_uint_32 l;
    png_bytep row_pointer[height];
    /* allocate space for the image */
    for (k = 0; k < height; k++) {
        /* row_pointer[k] = (png_byte *)(image + k * width); */
        row_pointer[k] = (png_byte *)malloc((size_t)rowbytes);
    }

    /* set up the image */
    l = 0;
    png_uint_32 greenvalue,redvalue,bluevalue ;
for(k=0;k<height;k++){
	for (l=0 ; l< width;l++){//this is wrong as is , thinkng
		*(row_pointer[k]+(l*3)) = windowbuffer[(k*width)+l].r;  //this couldn't have been width*3 'cause that would have screwed up your array
		*(row_pointer[k]+(l*3)+1) = windowbuffer[(k*width)+l].g;
		*(row_pointer[k]+(l*3)+2) = windowbuffer[(k*width)+l].b;//thinking... k*w?
	}
} 


pngout = fopen(newfilename, "wb");
if (!pngout) {
	cout <<"Ooo, couldn't open file:" << newfilename <<endl; //write something? = wb?
	return 1;
}
write_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, (void *)NULL, (png_error_ptr)NULL, (png_error_ptr)NULL);
if (!write_ptr) { fclose(pngout); return 1; }

info_ptr =  png_create_info_struct(write_ptr);
if (!info_ptr) {
	fclose(pngout);
	png_destroy_write_struct(&write_ptr, (png_infopp)NULL);
	cout <<"unalbe to create info struct"<<endl;
	return 1;
}
    /* set error handling */
    if (setjmp(write_ptr->jmpbuf)) {
        fclose(pngout);
        png_destroy_write_struct(&write_ptr, &info_ptr);
cout <<"bad error jump handlign" << endl;
        return 1;
    }
    /* set up the output code */
    png_init_io(write_ptr, pngout);
    /* fill in png_info structure */
    png_set_IHDR(write_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB,
       PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);
    /* write the header information to the file */
    png_write_info(write_ptr, info_ptr);
    /* swap bytes of 16-bit files to most significant byte first */
    /* png_set_swap(write_ptr); */

    /* write the entire image */
    png_write_image(write_ptr, row_pointer);
    png_write_end(write_ptr, info_ptr);
    /* free all memory allocated */
    png_destroy_write_struct(&write_ptr, (png_infopp)NULL);
    /* close the file */
    fclose(pngout);
    return 0;
}
