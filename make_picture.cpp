#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <iostream.h>
#include <gd.h>
#include <string.h>
#include "pixel.h"
#include "png.cc"


void main(int argc, char *argv[]) {


	int w = 300; 
	int h = 400; 
	int xsize = w;
	int ysize = h;
	int count;

	pixel *dst = new pixel[w*h](0,0,0);

	for(int t =0; t < (h*w); t++) {
		dst[t] = pixel(0,0,0);
	}

	count=0;

   DIR *dir = opendir("pictures/");
   dirent *ent;

	FILE *file;
	char *filename = new char[50];

	gdImagePtr ip;
	int val;

   while(ent = readdir(dir)) {
      if(strcmp("..", ent->d_name) && strcmp(".", ent->d_name)) {
			sprintf(filename, "pictures/%s", ent->d_name);
			cout << "Filename: " << filename << endl;
			file = fopen(filename, "r");
			ip = gdImageCreateFromJpeg(file);
			fclose(file);


			for(int y = 0; y < ysize; y++) {
				for(int x = 0; x < xsize; x++) {
					val = gdImageGetPixel(ip, x, y);
					dst[(y*xsize)+x].g += ip->green[val]; 
					dst[(y*xsize)+x].r += ip->red[val]; 
					dst[(y*xsize)+x].b += ip->blue[val]; 
				}
			}
			count++;
			gdImageDestroy(ip);
		}
	}

	for(int y = 0; y < ysize; y++) {
		for(int x = 0; x < xsize; x++) {
			dst[(y*xsize)+x].r = (int)(dst[(y*xsize)+x].r / count); 
			dst[(y*xsize)+x].g = (int)(dst[(y*xsize)+x].g / count); 
			dst[(y*xsize)+x].b = (int)(dst[(y*xsize)+x].b / count); 
		}
	}


	pngwrite(dst, w, h, "average_southerner.png");
	delete dst;
}
